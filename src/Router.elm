module Router
    ( match, arguments, address, exactly
    , (:=>), (./)
    ) where

{-|  Simple URL router that is capable of matching URL patterns, retrieving placeholder argument and reacting to changes in web browser URL address bar.

# Pattern
@docs exactly, (./), (:=>)

# Routing
@docs match, address, arguments
-}

import Maybe
import String exposing (split, startsWith, dropLeft)
import List exposing (append, length, filter, head, drop, map2, foldl, (::))

import Router.Types exposing (Page, Arguments)
import Router.InternalTypes exposing (..)
import Native.Url

{-| The current value of URL fragment identifier (hash parameter). -}
address : Signal String
address = Native.Url.address

{-| Join URL pattern part. -}
(./) : Pattern -> PatternPart -> Pattern
(./) parts part = parts `append` [part]

{-| Associates URL pattern with view. -}
(:=>) : Pattern -> View a b -> Page a b
(:=>) pattern view = {address = pattern, view = view}

{-| Denotes an exact value of URL path. 
For e.g:

    [exactly "about"] ./ exactly "company"

is is equivalent of `/about/company`.
-}
exactly : String -> PatternPart
exactly str = Exact str

{-| Normalize URL address be removing leading slash if given URL address has one. -}
normalize : String -> String
normalize url = if startsWith "/" url then dropLeft 1 url else url

{-| Remove pages that doesn't have matching address pattern. -}
discard : List (Page a b) -> String -> List (Page a b)
discard pages url =
    let
        urlParts = split "/" (normalize url)
        appropriateLength = filter (\p -> length p.address == length urlParts) pages
        get i from =
            drop i from |> head
        toBool result =
            case result of
                Ok v -> True
                Err err -> False
        check part value =
            case part of
                Exact s ->
                    s == value
                Placeholder trans ->
                    case trans value of
                        Characters cr ->
                            toBool cr
                        Digits dr ->
                            toBool dr
                        Decimal dr ->
                            toBool dr
                        NotPlaceholder ->
                            False
        validate part value =
            check (Maybe.withDefault (Exact "") part) (Maybe.withDefault "" value)
        sift i siftedPages =
            if i == length urlParts then
                siftedPages
            else
                sift (i+1) (filter (\p -> validate (get i p.address) (get i urlParts)) siftedPages)
    in
        sift 0 appropriateLength

{-| Find matching view for given URL address. If there is no matching view, return `notFound` view. -}
match : String -> List (Page a b) -> View a b -> View a b
match url pages notFound =
    let
        filteredPages = discard pages url
    in
        if length filteredPages == 1
            then head filteredPages `Maybe.andThen` (\p -> Just p.view) |> Maybe.withDefault notFound
            else notFound

{-| Retrieve URL arguments denoted by placeholders. -}
arguments : String -> List (Page a b) -> Arguments
arguments url pages =
    let
        filteredPages = discard pages url
        urlParts = split "/" (normalize url)
        nativeArguments = {strings = [], integers = [], floats = [], total = 0}

        extract address =
            let
                addressAndUrlParts = map2 (\a b -> (a, b)) address urlParts
                pick addressPartAndUrlPart args =
                    case fst addressPartAndUrlPart of
                        Exact s ->
                            args
                        Placeholder trans ->
                            case trans (snd addressPartAndUrlPart) of
                                Characters cr ->
                                    case cr of
                                        Ok v ->
                                            { args | strings = (args.total, v) :: args.strings, total = args.total + 1 }
                                        Err err ->
                                            args
                                Digits dr ->
                                    case dr of
                                        Ok v ->
                                            { args | integers = (args.total, v) :: args.integers, total = args.total + 1 }
                                        Err err ->
                                            args
                                Decimal dr ->
                                    case dr of
                                        Ok v ->
                                            { args | floats = (args.total, v) :: args.floats, total = args.total + 1 }
                                        Err err ->
                                            args
                                NotPlaceholder ->
                                    args
            in
                foldl pick nativeArguments addressAndUrlParts
    in
        if length filteredPages == 1
            then
                case head filteredPages of
                    Just page ->
                        extract page.address
                    Nothing ->
                        nativeArguments
            else nativeArguments
