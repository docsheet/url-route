module Router.Types
    ( Page, Arguments
    ) where

{-| Publicly available types. 

@docs Page, Arguments

-}

import Router.InternalTypes exposing (..)

{-| Represents association of URL pattern with a view. -}
type alias Page a b =
    { address : Pattern
    , view : View a b
    }

{-| Collection of transformed URL arguments. -}
type alias Arguments =
    { strings : List StrArgument
    , integers : List IntArgument
    , floats : List FloatArgument
    , total : Int
    }
