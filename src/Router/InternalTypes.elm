module Router.InternalTypes
    ( PlaceholderResult(..), PatternPart(..)
    , Transformer, StrArgument, IntArgument
    , FloatArgument, Pattern
    , View
    ) where

{-| Types definition used internally in this package. -}

{-| Represents result of an placeholder transformation to one of the Elm native types. -}
type PlaceholderResult =
    Characters (Result String String)
    | Digits (Result String Int)
    | Decimal (Result String Float)
    | NotPlaceholder

{-| Function that transforms `String` value into Elm type. -}
type alias Transformer = (String -> PlaceholderResult)

{-| Part of URL pattern. -}
type PatternPart = Exact String | Placeholder Transformer

{-| URL pattern. -}
type alias Pattern = List PatternPart

{-| Represents transformed URL argument (`String`) and its position. -}
type alias StrArgument = (Int, String)
{-| Represents transformed URL argument (`Int`) and its position. -}
type alias IntArgument = (Int, Int)
{-| Represents transformed URL argument (`Float`) and its position. -}
type alias FloatArgument = (Int, Float)

{-| View that is assigned to an URL pattern. -}
type alias View a b = a -> b
