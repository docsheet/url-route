module Router.Placeholders
    ( int, float, choice
    ) where

{-| Elements used to denote position of URL arguments in URL pattern.
Additionally each placeholder transforms argument value from `String` to Elm type.

@docs int, float, choice

-}

import String
import List exposing (member)

import Router.InternalTypes exposing (..)

{-| Allow only one of the given values to be present in the corresponding position of an URL address. -}
choice : List String -> PatternPart
choice options =
    let
        check s =
            if member s options then
                Characters (Ok s)
            else
                Characters (Err "No valid option has been provided")
    in
        Placeholder check

{-| Allow only integer to be present in the corresponding position of an URL address. -}
int : PatternPart
int =
    let
        check s =
            Digits (String.toInt s)
    in
        Placeholder check

{-| Allow only float value to be present in the corresponding position of an URL address. -}
float : PatternPart
float =
    let
        check s =
            Decimal (String.toFloat s)
    in
        Placeholder check
