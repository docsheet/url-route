module Router.Arguments 
    ( strAt, intAt, floatAt
    ) where

{-| Set of tools that helps in retrieving particular URL argument in native Elm type.
Position of each argument is counted from 0. Only placeholders are counted as index offset.

@docs strAt, intAt, floatAt

-}

import List exposing (head, filter)

import Router.Types exposing (Arguments)

{-| Retrieve `String` from a given position. -}
strAt : Arguments -> Int -> Maybe String
strAt args position = 
    if args.total == 0  then
        Nothing
    else
        case filter (\sa -> fst sa == position) args.strings |> head of
            Just sa -> Just (snd sa)
            Nothing -> Nothing 

{-| Retrieve `Int` from a given position. -}
intAt : Arguments -> Int -> Maybe Int
intAt args position =
    if args.total == 0  then
        Nothing
    else
        case filter (\ia -> fst ia == position) args.integers |> head of
            Just ia -> Just (snd ia)
            Nothing -> Nothing 

{-| Retrieve `Float` from a given position. -}
floatAt : Arguments -> Int -> Maybe Float
floatAt args position =
    if args.total == 0  then
        Nothing
    else
        case filter (\fa -> fst fa == position) args.floats |> head of
            Just fa -> Just (snd fa)
            Nothing -> Nothing
