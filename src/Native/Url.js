Elm.Native.Url = {};
Elm.Native.Url.make = function (elm) {
    elm.Native = elm.Native || {};
    elm.Native.Url = elm.Native.Url || {};

    if (elm.Native.Url.values) return elm.Native.Url.values;

    var Signal = Elm.Native.Signal.make(elm);

    var address = Signal.input("Url.address", stripHash(window.location.hash));

    window.onhashchange = function () {
        elm.notify(address.id, stripHash(window.location.hash));
    };

    return elm.Native.Url.values = {
        address: address
    };
};

function stripHash (hash) {
    return hash.slice(1, hash.length);
}